
import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        byte theByte = 23;
        long milisecond = System.currentTimeMillis();

        String name = new String("sdfsdf");

        System.out.println(name);
        System.out.println(milisecond);
        System.out.println("Hello, World!");

        String gender = "male";

        switch (gender.toUpperCase()) {
            case "FEMALE": {
                System.out.println("I am a female");
                break;
            }

            case "MALE": {
                System.out.println("I am a male");
                break;
            }
            default:
                System.out.println("Unknown gender");
        }

        int[] numbers = { 2, 0, 1, 4, 100, 4, 90, 78, 77 };

        // numbers.for
        for (int numbers2 : numbers) {

        }

        // numbers.fori
        for (int numbers2 = 0; numbers2 < numbers.length; numbers2++) {

        }

        // functional way of looping an array
        Arrays.stream(numbers).forEach(System.out::println);

    }
}
